//
//  QuizAppDelegate.h
//  Quiz
//
//  Created by アリャビマ　アウリア ラーマン on 12/09/24.
//  Copyright (c) 2012年 アリャ. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QuizViewController;

@interface QuizAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) QuizViewController *viewController;

@end
