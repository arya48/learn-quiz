//
//  main.m
//  Quiz
//
//  Created by アリャビマ　アウリア ラーマン on 12/09/24.
//  Copyright (c) 2012年 アリャ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QuizAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([QuizAppDelegate class]));
    }
}
