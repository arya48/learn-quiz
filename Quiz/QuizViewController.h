//
//  QuizViewController.h
//  Quiz
//
//  Created by アリャビマ　アウリア ラーマン on 12/09/24.
//  Copyright (c) 2012年 アリャ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuizViewController : UIViewController
{
    int currentQuestionIndex;
    
    //model objects
    NSMutableArray *questions;
    NSMutableArray *answers;
    
    //view objects
    IBOutlet UILabel *questionField;
    IBOutlet UILabel *answerField;
}

- (IBAction)showQuestion:(id)sender;
- (IBAction)showAnswer:(id)sender;
@end
