//
//  QuizViewController.m
//  Quiz
//
//  Created by アリャビマ　アウリア ラーマン on 12/09/24.
//  Copyright (c) 2012年 アリャ. All rights reserved.
//

#import "QuizViewController.h"

@interface QuizViewController ()

@end

@implementation QuizViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    //class the ini method implemented by the superclass
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //create 2 arrays and make the pointer to them
        questions = [[NSMutableArray alloc] init];
        answers = [[NSMutableArray alloc] init];
        
        //add questions and answers to array
        [questions addObject:@"7+'=?"];
        [answers addObject:@"14"];
        
        [questions addObject:@"B5"];
        [answers addObject:@"Goddess of Theater"];
        
        [questions addObject:@"A1"];
        [answers addObject:@"The Party will start"];
    }
    //return the address of the new object
    return self;
}

- (IBAction)showQuestion:(id)sender
{
    //step to next question
    currentQuestionIndex++;
    
    //am i past the last question
    if (currentQuestionIndex == [questions count]) {
        //go back to first question
        currentQuestionIndex = 0;
    }
    
    //get string at the index in the question array
    NSString *question = [questions objectAtIndex:currentQuestionIndex];
    
    //log the string to the console
    NSLog(@"displating question: %@", question);
    
    //display the string in the question field
    [questionField setText:question];
    
    //cleare the answer field
    [answerField setText:@"???"];
    
}

- (IBAction)showAnswer:(id)sender
{
    //answer of current question
    NSString *answer = [answers objectAtIndex:currentQuestionIndex];
    
    //display the answer
    [answerField setText:answer];
}
@end
